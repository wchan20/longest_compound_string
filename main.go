package main

import (
	"flag"
	"io/ioutil"
	"log"
	"strings"

	"github.com/wchan2/longest_compound_string/compound_string_processor"
	"github.com/wchan2/longest_compound_string/map_reduce"
)

var (
	threads  *int
	filename *string
)

func init() {
	threads = flag.Int("threads", 1, "specifies the number of *threads to run")
	filename = flag.String("file", "", "specifies the file to load to find the longest compound word")
	flag.Parse()
	if *filename == "" {
		log.Fatal("please specify the file flag to seed the words directory to find the longest compound word")
	}

}

func readDataFromFile(filename string) []string {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err.Error())
	}
	return strings.Split(string(data), "\n")
}

func main() {
	words := readDataFromFile(*filename)
	processes := make([]map_reduce.Process, *threads)

	log.Printf("creating %d processes", *threads)
	for i := 0; i < *threads; i++ {
		processes[i] = compound_string_processor.NewCompoundStringProcess(
			map_reduce.NewStringGenerator(map_reduce.PartitionData(words, *threads)[i]),
			words,
		)
	}

	result := make(chan string)
	defer close(result)
	mapReduce := compound_string_processor.NewCompoundStringMapReduce(result, processes...)
	mapReduceManager := map_reduce.NewMapReduceManager(mapReduce)
	mapReduceManager.Run()

	log.Printf("the longest compound word is %s", <-result)
}
