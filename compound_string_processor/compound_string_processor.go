package compound_string_processor

type CompoundStringProcessor struct {
	words []string
}

func NewCompoundStringProcessor(words []string) *CompoundStringProcessor {
	return &CompoundStringProcessor{words: words}
}

func (c *CompoundStringProcessor) IsCompoundString(str string) bool {
	return c.containsCompoundString(str, str)
}

func (c *CompoundStringProcessor) containsCompoundString(str string, excludes string) bool {
	for i := 1; i <= len(str); i++ {
		for _, word := range c.words {
			subWord := str[i:len(str)]

			foundAtEndOfString := word == str[0:i] && len(subWord) == 0
			foundCompoundWord := word == str[0:i] && c.containsCompoundString(subWord, excludes)

			if (foundAtEndOfString || foundCompoundWord) && str[0:i] != excludes {
				return true
			}
		}
	}
	return false
}
