package compound_string_processor_test

import (
	"github.com/wchan2/longest_compound_string/compound_string_processor"

	"testing"
)

func TestCompoundStringProcess(t *testing.T) {
	input := make(chan string)

	regularCompoundString := "hello"
	compoundStringAndWholeString := "war"
	compoundStringWithMultipleMatches := "wor"
	strings := []string{
		regularCompoundString,
		"non-compound-string",
		compoundStringAndWholeString,
		"zor",
		compoundStringWithMultipleMatches,
		"ll",
	}

	go func() {
		defer close(input)
		for i := range strings {
			input <- strings[i]
		}
	}()

	process := compound_string_processor.NewCompoundStringProcess(input, []string{"he", "ll", "o", "w", "or", "ld", "foob", "ar", "war"})
	process.Start()
	expected := []string{regularCompoundString, compoundStringAndWholeString, compoundStringWithMultipleMatches}
	output := process.GetOutputChannel()
	results := []string{}

	for compoundWord := range output {

		results = append(results, compoundWord)
	}
	if len(results) != len(expected) {
		t.Errorf("expected results len: %d, got: %d", len(expected), len(results))
	}

	for i := range results {
		if expected[i] != results[i] {
			t.Errorf("expected: %s, got: %s", expected, results)
		}
	}
}
