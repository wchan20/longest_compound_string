package compound_string_processor_test

import (
	"io/ioutil"
	"math/rand"
	"strings"
	"testing"

	"github.com/wchan2/longest_compound_string/compound_string_processor"
)

func TestCompoundStringProcessor(t *testing.T) {
	processor := compound_string_processor.NewCompoundStringProcessor([]string{"he", "ll", "o", "w", "or", "ld", "foob", "ar", "war"})

	regularCompoundStrings := []string{"hello", "foobar", "hell", "heo", "wld", "hellld", "hear", "foobarld"}
	for _, word := range regularCompoundStrings {
		if !processor.IsCompoundString(word) {
			t.Errorf("expected %s to be compound string, but returned false", word)
		}
	}

	compoundStringAndWholeStrings := []string{"war"}
	for _, word := range compoundStringAndWholeStrings {
		if !processor.IsCompoundString(word) {
			t.Errorf("expected %s to be compound string, but returned false", word)
		}
	}

	compoundStringWithMultipleMatches := []string{"world", "wor", "oworld"}
	for _, word := range compoundStringWithMultipleMatches {
		if !processor.IsCompoundString(word) {
			t.Errorf("expected %s to be compound string, but returned false", word)
		}
	}

	nonCompoundStrings := []string{"hel", "fooba", "well", "z", "zwor"}
	for _, word := range nonCompoundStrings {
		if processor.IsCompoundString(word) {
			t.Errorf("expected %s to not be compound string, but returned true", word)
		}
	}

	exactStrings := []string{"he", "ll", "o", "w", "or", "ld", "foob", "ar"}
	for _, word := range exactStrings {
		if processor.IsCompoundString(word) {
			t.Errorf("expected %s to not be compound string, but returned true", word)
		}
	}
}

func BenchmarkCompoundStringProcessor(b *testing.B) {
	data, err := ioutil.ReadFile("../word.list")
	if err != nil {
		b.Fatal(err.Error())
	}
	words := strings.Split(string(data), "\n")
	processor := compound_string_processor.NewCompoundStringProcessor(words)
	for i := 0; i < b.N; i++ {
		processor.IsCompoundString(words[rand.Intn(len(words))])
	}
}
