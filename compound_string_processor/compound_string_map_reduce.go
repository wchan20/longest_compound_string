package compound_string_processor

import (
	"log"

	"github.com/wchan2/longest_compound_string/map_reduce"
)

type CompoundStringMapReduce struct {
	processes    []map_reduce.Process
	outputs      []<-chan string
	mergeChannel chan string
	result       chan string
}

func NewCompoundStringMapReduce(result chan string, processes ...map_reduce.Process) *CompoundStringMapReduce {
	mapReduce := &CompoundStringMapReduce{
		result:       result,
		processes:    processes,
		mergeChannel: make(chan string),
		outputs:      make([]<-chan string, len(processes)),
	}
	for i := 0; i < len(processes); i++ {
		mapReduce.outputs[i] = processes[i].GetOutputChannel()
	}
	return mapReduce
}

func (c *CompoundStringMapReduce) Map() {
	log.Print("starting processes")
	for i := range c.processes {
		c.processes[i].Start()
	}
}

func (c *CompoundStringMapReduce) Reduce() {
	for i := range c.outputs {
		go func(results <-chan string) {
			defer close(c.mergeChannel)
			for result := range results {
				c.mergeChannel <- result
			}
		}(c.outputs[i])
	}

	go func() {
		longestCompoundWord := ""
		for compoundWord := range c.mergeChannel {
			log.Printf("read compound word %s from output", compoundWord)
			if len(compoundWord) > len(longestCompoundWord) {
				log.Printf("update longest compound word from %s to %s", longestCompoundWord, compoundWord)
				longestCompoundWord = compoundWord
			}
		}
		c.result <- longestCompoundWord
	}()
}
