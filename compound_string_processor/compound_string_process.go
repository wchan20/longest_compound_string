package compound_string_processor

import (
	"log"
)

var compoundStringProcessor *CompoundStringProcessor

type CompoundStringProcess struct {
	input  <-chan string
	output chan string

	*CompoundStringProcessor
}

func NewCompoundStringProcess(input <-chan string, words []string) *CompoundStringProcess {
	if compoundStringProcessor == nil {
		compoundStringProcessor = NewCompoundStringProcessor(words)
	}

	return &CompoundStringProcess{
		input:                   input,
		output:                  make(chan string),
		CompoundStringProcessor: compoundStringProcessor,
	}
}

func (c *CompoundStringProcess) Start() {
	go func() {
		defer close(c.output)

		log.Printf("process %#v is started", c)
		for str := range c.input {
			log.Printf("read string from input: %s", str)
			if c.IsCompoundString(str) {
				log.Printf("%s is a compound string", str)
				c.output <- str
			}
		}
	}()
}

func (c *CompoundStringProcess) GetOutputChannel() <-chan string {
	return c.output
}
