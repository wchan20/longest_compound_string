## Longest Compound Word

The following program takes a file and a thread count and processes the words in the file to look for the longest compounded word. 

### Optimizations

Some optimizations that could be done includes: 

- Only looking at words that are shorter than the word that can potentially make up the word under test.
- Getting an updated copy and comparing the length under test before attempting to do a compounded word lookup.

### Running the program

There are two ways to run the program. One way is to use the `go run main.go` when in the `longest_compound_string` directory and another is to build the binary in the in the directory as seen [here](#building-the-binary) and then running the binary that was built [here](#running-the-binary). Below is the command to run program without building it.

Generally, there are 2 flags and they are:

- `file` (Required) - the name of the file for which to read from to find the longest compound word
- `threads` (Optional) - the number of threads to execute the program under and defaults to 1 if not provided

```
go run main.go -file <name_of_file> -threads <number_of_threads_to_run>
```

### Building the binary

```
cd path/to/directory
go build .
```

### Running the binary

The program takes 2 flags and they are:

- `file` (Required) - the name of the file for which to read from to find the longest compound word
- `threads` (Optional) - the number of threads to execute the program under and defaults to 1 if not provided

```
./longest_compound_string -file <name_of_file> -threads <number_of_threads_to_run>
```

### Running the tests

```
go test ./...
```

### Running the benchmarks

There is an included benchmark test for finding a compound string for a given word in a haystack of other words. When inside the `compound_string_proecessor` directory, run the below command.

```
go test -bench=.
```

### Example results

The following is an example benchmark result ran on a Macbook Pro 2.6 GHz Intel Core i5. It equates to ~0.03 seconds per operation on a single CPU. For the entire word list being the size of 263,533, it would take about 2.2 hours to process the entire file looking for compounded words without any other optimizations on a single core.

Some optimizations are suggested in [this](#optimizations) section.

```
BenchmarkCompoundStringProcessor	     500	  34044482 ns/op
```
