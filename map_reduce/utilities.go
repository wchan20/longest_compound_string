package map_reduce

func PartitionData(data []string, partitions int) [][]string {
	dataPartitions := make([][]string, partitions)
	partitionSize := len(data) / partitions
	for i := 0; i < partitions-1; i++ {
		dataPartitions[i] = data[i*partitionSize : (i+1)*partitionSize]
	}
	dataPartitions[partitions-1] = data[(partitions-1)*partitionSize : len(data)]
	return dataPartitions
}

func NewStringGenerator(data []string) <-chan string {
	input := make(chan string)
	go func() {
		for i := range data {
			input <- data[i]
		}

		close(input)
	}()

	return input
}
