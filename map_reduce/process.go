package map_reduce

type Process interface {
	Start()
	GetOutputChannel() <-chan string
}
