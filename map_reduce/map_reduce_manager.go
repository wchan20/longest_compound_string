package map_reduce

type MapReduceJob interface {
	Map()
	Reduce()
}

type MapReduceManager struct {
	jobs []MapReduceJob
}

func NewMapReduceManager(jobs ...MapReduceJob) *MapReduceManager {
	return &MapReduceManager{jobs: jobs}
}

func (m *MapReduceManager) Run() {
	for i := range m.jobs {
		m.jobs[i].Map()
		m.jobs[i].Reduce()
	}
}
